from rest_framework import generics

from ..models import OurProjects
from ..serializers import OurProjectsSerializer


class OurProjectsAPIView(generics.ListAPIView):
    queryset = OurProjects.objects.all()
    serializer_class = OurProjectsSerializer
