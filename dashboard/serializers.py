from rest_framework import serializers

from .models import OurProjects


class OurProjectsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OurProjects
        fields = ('name', 'description', 'link', 'image', 'date')
