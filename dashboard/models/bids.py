from datetime import datetime

from django.db import models


class Bid(models.Model):
    description: str = models.TextField(max_length=200)
    email: str = models.CharField(max_length=50)
    date: datetime = models.DateTimeField()
