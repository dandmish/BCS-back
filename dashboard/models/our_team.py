from django.db import models


class OurTeam(models.Model):
    name: str = models.CharField(verbose_name="Имя", max_length=25)
    surname: str = models.CharField(verbose_name="Фамилия", max_length=25)
    patronymic: str = models.CharField(verbose_name="Отчество", max_length=25)
    role: str = models.CharField(verbose_name="Роль сотрудника", max_length=25)
    image: models.ImageField = models.ImageField(verbose_name="Фото сотрудника", upload_to='our_team')

    @property
    def get_fio(self):
        return f"{self.surname} {self.name} {self.patronymic}"
