from datetime import datetime

from django.db import models


class OurProjects(models.Model):
    name: str = models.CharField(max_length=50)
    description: str = models.TextField(max_length=200)
    image: models.ImageField = models.ImageField(upload_to='our_projects')
    date: datetime = models.DateTimeField()
    link: str = models.CharField(max_length=100)
