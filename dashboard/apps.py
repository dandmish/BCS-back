from django.apps import AppConfig


class DashboardAppConfig(AppConfig):
    name = "dashboard"
    verbose_name = "Главная страница"
