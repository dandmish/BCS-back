from django.contrib import admin

from ..models import OurProjects


@admin.register(OurProjects)
class OurProjectsAdmin(admin.ModelAdmin):
    list_display = (
        "name",
    )
