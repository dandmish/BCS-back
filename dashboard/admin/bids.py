from django.contrib import admin

from ..models import Bid


@admin.register(Bid)
class BidsAdmin(admin.ModelAdmin):
    list_display = (
        "description",
    )
