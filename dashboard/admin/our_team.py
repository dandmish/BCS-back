from django.contrib import admin

from ..models import OurTeam


@admin.register(OurTeam)
class OurTeamAdmin(admin.ModelAdmin):
    list_display = (
        "get_fio",
    )
